+++
title = "Plainpicture"
categories = ["sysadmin"]
coders = ""
description = "System administration."
github = ["https://github.com/plainpicture"]
site = "https://www.plainpicture.com"
image = "https://www.plainpicture.com/assets/logo-3b9bdf351e28edd305b63a48eef634e5b1f10231e7b88de9b6fd99a29a664498.png"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Chef_logo.svg/200px-Chef_logo.svg.png"
name = "Chef"
url = "https://www.chef.io"

#[[tech]]
#logo = ""
#name = ""
#url = ""

[[tech]]
logo = "http://boxchronicles.com/wp-content/uploads/2015/01/ruby.png"
name = "Ruby"
url = "https://www.ruby-lang.org/"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/220px-Ruby_On_Rails_Logo.svg.png"
name = "Ruby on rails"
url = "https://rubyonrails.org/"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg"
name = "Docker"
url = "https://www.docker.com"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Icinga_logo.svg/250px-Icinga_logo.svg.png"
name = "Icinga"
url = "https://icinga.com"

[[tech]]
name = "Cassandra"
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Cassandra_logo.svg/100px-Cassandra_logo.svg.png"
url = "https://cassandra.apache.org/"

[[tech]]
name = "Elasticsearch"
logo = "https://images.contentstack.io/v3/assets/bltefdd0b53724fa2ce/blt280217a63b82a734/5bbdaacf63ed239936a7dd56/elastic-logo.svg"
url = "https://www.elastic.co/de/elasticsearch/"

[[tech]]
name = "HAProxy"
logo = "https://www.haproxy.org/img/HAProxyCommunityEdition_60px.png"
url = "https://www.haproxy.org/"

[[tech]]
name = "Kafka"
logo = "http://kafka.apache.org/images/apache-kafka.png"
url = "https://kafka.apache.org/"

[[tech]]
name = "Percona MySQL"
logo = "https://www.percona.com/sites/default/files/logo_0_1.png"
url = "https://www.percona.com"

[[tech]]
name = "Redis"
logo = "https://upload.wikimedia.org/wikipedia/en/thumb/6/6b/Redis_Logo.svg/300px-Redis_Logo.svg.png"
url = "https://redis.io/"

[[tech]]
name = "Zookeeper"
logo = "https://zookeeper.apache.org/images/zookeeper_small.gif"
url = "https://zookeeper.apache.org/"

# [[tech]]
# name = ""
# logo = ""
# url = ""

+++

I'm currently working as a sysadmin for Plainpicture since 2018.
