---
title: "Hardware seeks hacker"
# type: post
tags: ["etc", "hardware"]
description: "Stuff I'd like to get rid of"
date: 2025-02-18
---

Can bring to next CCC Congress or [contact me](https://www.varac.net/contact/)
in any case.

## Give away for free

- MSI Bluetooth USB dongle
- Vodafone HSDPA (+UMTS EDGE GPRS) USB Stick (Model K3520)
- CPU-FAN new, not used, Socket AM3+
- 2-Port USB2.0 hub
- Conrad energy 6V 3.2Ah rechargeable Battery (nearly new)
- 1x Dect basestation Gigaset SL100
- Kobil TAN Optimus comfort TAN generator hck0f00-vr

### Input

- Logitech Webcam V-UBK45
- Logitech M-RCQ142 Bluetooth mouse
- Apple Keyboards (all engl. layout)
  - 2x A1243
  - 1 broken A1243 for key caps replacements

### Wifi

- Alpha Networks AWUS036NHA USB adapter (Atheros AR9271, 802.11b/g/n)
- TP Link TL-WN722N USB adapter (150Mbps)
- M2 Wifi/Bluetooth card AW-CB304NF

## To sell

Please propose a price. If you can't afford any money please feel free to reach
out for me anyways.

- 2x HP ProCurve Switch 2900-48G (J9050A)
- Evoluent VM4RB VerticalMouse 4 Right Bluetooth mouse
- Waveshare 5inch DSI LCD touch display
- Telekom Speedport W723V Typ A
- Telekom Speedport Smart 3
